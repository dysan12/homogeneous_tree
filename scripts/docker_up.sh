#!/usr/bin/env bash

CURR_PATH=$(pwd)

if pwd | grep -q "/images" || pwd | grep -q "/scripts"
    then MAIN_PATH="${CURR_PATH}/.."
    else MAIN_PATH=${CURR_PATH}
fi

cd "${MAIN_PATH}/images"

docker-compose up -d

sh "$MAIN_PATH/scripts/codeception_build.sh"
sh "$MAIN_PATH/scripts/composer_install.sh"