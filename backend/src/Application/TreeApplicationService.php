<?php
/**
 * User: Michal Gaj
 * Date: 27.10.18
 * E-mail: michalgaj@onet.pl
 */

namespace App\Application;


use App\Domain\Graph\Service\GraphService;
use App\Domain\Graph\Service\SvgConfigInterface;
use App\Domain\Graph\Service\XmlConfigInterface;
use App\Domain\Tree\Service\TreeParametersInterface;
use App\Domain\Tree\Service\TreeService;

class TreeApplicationService
{
    /**
     * @var GraphService
     */
    private $graphService;
    /**
     * @var TreeService
     */
    private $treeService;

    public function __construct(GraphService $graphService, TreeService $treeService)
    {
        $this->graphService = $graphService;
        $this->treeService = $treeService;
    }

    /**
     * @param SvgConfigInterface $svgConfig
     * @param TreeParametersInterface $treeParameters
     * @param array $treeCommands
     * @return string
     * @throws \App\Domain\Exception\CommandExecution
     * @throws \App\Domain\Exception\CommandNotSupported
     */
    public function getSvgTreeRepresentation(SvgConfigInterface $svgConfig, TreeParametersInterface $treeParameters, array $treeCommands = []): string
    {
        $tree = $this->treeService->createTree($treeParameters, $treeCommands);
        $graph = $this->graphService->generateSvgGraph($tree, $svgConfig);

        return $graph->getRepresentation();
    }

    /**
     * @param XmlConfigInterface $xmlConfig
     * @param TreeParametersInterface $treeParameters
     * @param array $treeCommands
     * @return string
     * @throws \App\Domain\Exception\CommandExecution
     * @throws \App\Domain\Exception\CommandNotSupported
     */
    public function getXmlTreeRepresentation(XmlConfigInterface $xmlConfig, TreeParametersInterface $treeParameters, array $treeCommands = []): string
    {
        $tree = $this->treeService->createTree($treeParameters, $treeCommands);
        $graph = $this->graphService->generateXmlGraph($tree, $xmlConfig);

        return $graph->getRepresentation();
    }
}