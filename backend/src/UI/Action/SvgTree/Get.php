<?php
/**
 * User: michal_lap
 * Date: 18.11.18
 * Email: michalgaj@onet.pl
 */

namespace App\UI\Action\SvgTree;

use App\Application\TreeApplicationService;
use App\Domain\Exception\CommandExecution;
use App\UI\DTO;
use PhpOffice\PhpWord\PhpWord;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\HeaderUtils;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class Get
{
    /**
     * @Route(
     *     path="/tree/svg",
     *     methods={"POST"}
     * )
     */
    public function __invoke(Request $request, ValidatorInterface $validator, TreeApplicationService $treeApplicationService)
    {
        $body = json_decode($request->getContent(), true) ?? [];
        $svgTreeData = DTO\SvgTree::createFromArray($body);

        $violations = $validator->validate($svgTreeData);

        if (0 === \count($violations)) {
            try {
                $tree = $treeApplicationService->getSvgTreeRepresentation(
                    $svgTreeData->getConfig(),
                    $svgTreeData->getTreeParameters(),
                    $svgTreeData->getCommands()
                );

                $acceptedTypes = $this->getAcceptedTypes($request);
                for ($i = 0; $i < count($acceptedTypes) && false === isset($response); $i++) {
                    $acceptedType = $acceptedTypes[$i];
                    switch ($acceptedType) {
                        case 'application/pdf':
                            //consider better way of getting size of svg
                            if (preg_match('/<svg width="(?<width>[\d]+(?:[.,][\d]+)?)" height="(?<height>[\d]+(?:[.,][\d]+)?)">/', $tree, $matches)) {
                                $format = [ceil($matches['width']), ceil($matches['height'])];
                            } else {
                                $format = 'A4';
                            }

                            $response = new BinaryFileResponse($this->convertToPdf($tree, $format));
                            break;
                        case 'application/vnd.openxmlformats-officedocument.wordprocessingml.document':
                            $response = new BinaryFileResponse($this->convertToDocx($tree), Response::HTTP_OK, [
                                'Content-Type' => 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
                                'Content-Disposition' => 'attachment; filename="graph.docx"'
                            ]);
                            break;
                        case 'image/svg+xml':
                        case '*/*':
                            $response = new Response($tree, Response::HTTP_OK, ['Content-Type' => 'image/svg+xml']);
                            break;
                    }
                }

                if (false === isset($response)) {
                    $response = new Response('', Response::HTTP_NOT_ACCEPTABLE);
                }
            } catch (CommandExecution $exception) {
                $response = new JsonResponse(json_encode([
                    'errors' => ['One of provided commands is incorrect']
                ]), Response::HTTP_BAD_REQUEST);
            }
        } else {
            $errors = DTO\Errors::createFromViolationsList($violations);
            $response = new JsonResponse(json_encode($errors), Response::HTTP_BAD_REQUEST);
        }

        return $response;
    }

    //todo seperate it to class
    private function convertToPdf(string $tree, $format): string
    {
        $svgTmpFilename = @tempnam('/svg', 'svg');
        $svgTmpFile = fopen($svgTmpFilename, 'wb');
        fwrite($svgTmpFile, $tree);

        $pdf = new \TCPDF('L', 'px', $format);
        $pdf->setPrintHeader(false);
        $pdf->setPrintFooter(false);
        $pdf->AddPage();
        $pdf->ImageSVG($svgTmpFilename);


        $pdfTmpFileName = @tempnam('/pdf', 'pdf');
        $pdf->Output($pdfTmpFileName, 'F');

        return $pdfTmpFileName;
    }

    //todo seperate it to class
    private function convertToDocx(string $tree): string
    {
        $svgFilePath = @tempnam('/svg', 'svg');

        $svgFile = fopen($svgFilePath, 'wb');
        fwrite($svgFile, $tree);

        $pngFilePath = @tempnam('/png', 'png');
        $res = exec(sprintf('inkscape -z -e %s %s 2> /dev/null',$pngFilePath, $svgFilePath));

        $phpWord = new PhpWord();
        $section = $phpWord->addSection();

        $section->addImage($pngFilePath);

        $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
        $docxTmpFile = @tempnam('/docx', 'docx');
        $objWriter->save($docxTmpFile);

        return $docxTmpFile;
    }

    private function getAcceptedTypes(Request $request): array
    {
        $types = HeaderUtils::split($request->headers->get('Accept', 'image/svg+xml'), ',;');
        foreach ($types as $key => $type) {
            $types[$key] = $type[0];
        }

        return $types;
    }
}
