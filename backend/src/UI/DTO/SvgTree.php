<?php
/**
 * User: michal_lap
 * Date: 27.11.18
 * Email: michalgaj@onet.pl
 */

namespace App\UI\DTO;

use Symfony\Component\Validator\Constraints as Assert;

class SvgTree
{
    /**
     * @var TreeParameters
     * @Assert\NotNull()
     * @Assert\Valid()
     */
    private $treeParameters;
    /**
     * @var SvgGraphConfig
     * @Assert\NotNull()
     * @Assert\Valid()
     */
    private $config;

    /**
     * @var array
     * @Assert\Valid()
     */
    private $commands = [];

    /**
     * @return TreeParameters
     */
    public function getTreeParameters(): TreeParameters
    {
        return $this->treeParameters;
    }

    /**
     * @return SvgGraphConfig
     */
    public function getConfig(): SvgGraphConfig
    {
        return $this->config;
    }

    /**
     * @return array
     */
    public function getCommands(): array
    {
        return $this->commands;
    }

    /**
     * @param TreeParameters $treeParameters
     */
    public function setTreeParameters(TreeParameters $treeParameters): void
    {
        $this->treeParameters = $treeParameters;
    }

    /**
     * @param SvgGraphConfig $config
     */
    public function setConfig(SvgGraphConfig $config): void
    {
        $this->config = $config;
    }

    public static function createFromArray(array $data): self
    {
        $svgTree = new self;

        // todo divide it to separate methods
        $config = new SvgGraphConfig();
        if (isset($data['config']['nodeHeight'])) {
            $config->setNodeHeight((float)$data['config']['nodeHeight']);
        }
        if (isset($data['config']['nodeWidth'])) {
            $config->setNodeWidth((float)$data['config']['nodeWidth']);
        }
        if (isset($data['config']['gapHorizontalSize'])) {
            $config->setGapHorizontalSize((float)$data['config']['gapHorizontalSize']);
        }
        if (isset($data['config']['gapVerticalSize'])) {
            $config->setGapVerticalSize((float)$data['config']['gapVerticalSize']);
        }
        $svgTree->config = $config;

        $parameters = new TreeParameters();
        if (isset($data['treeParameters']['treeDepth'])) {
            $parameters->setTreeDepth((int)$data['treeParameters']['treeDepth']);
        }
        if (isset($data['treeParameters']['treeBranchSize'])) {
            $parameters->setTreeBranchSize((int)$data['treeParameters']['treeBranchSize']);
        }
        $svgTree->setTreeParameters($parameters);

        if (isset($data['commands']['swap'])) {
            foreach ($data['commands']['swap'] as $swap) {
                $command = new SwapCommand();
                if (isset($swap['level'])) {
                    $command->setLevel((int)$swap['level']);
                }
                if (isset($swap['nodeNo'])) {
                    $command->setNodeNo((int)$swap['nodeNo']);
                }
                if (isset($swap['sequence']) && \is_array($swap['sequence'])) {
                    $command->setSequence($swap['sequence']);
                }
                $svgTree->commands[] = $command;
            }
        }

        return $svgTree;
    }
}