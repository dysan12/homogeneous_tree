<?php
/**
 * User: michal_lap
 * Date: 25.11.18
 * Email: michalgaj@onet.pl
 */

namespace App\UI\DTO;


use App\Common\Collection\PositiveNumbersCollection;
use App\Domain\Tree\Node\VO\Position;
use App\Domain\Tree\Operation\Swap\Sequence;
use App\Domain\Tree\Service\Command\SwapInterface;
use Symfony\Component\Validator\Constraints as Assert;

class SwapCommand implements SwapInterface
{
    /**
     * @var int
     * @Assert\NotBlank()
     * @Assert\GreaterThanOrEqual(0)
     */
    private $level;
    /**
     * @var int
     * @Assert\NotBlank()
     * @Assert\GreaterThan(0)
     */
    private $nodeNo;
    /**
     * @var array
     * @Assert\NotBlank()
     * @Assert\All(
     *     @Assert\GreaterThan(0),
     *     @Assert\Type("int")
     * )
     */
    private $sequence;

    /**
     * @param int $level
     */
    public function setLevel(int $level): void
    {
        $this->level = $level;
    }

    /**
     * @param int $nodeNo
     */
    public function setNodeNo(int $nodeNo): void
    {
        $this->nodeNo = $nodeNo;
    }

    /**
     * @param array $sequence
     */
    public function setSequence(array $sequence): void
    {
        $this->sequence = $sequence;
    }

    public function getPosition(): Position
    {
        // object from domain ? :(
        return Position::createFromInts($this->level, $this->nodeNo);
    }

    public function getSequence(): Sequence
    {
        // object from domain ? :(
        return new Sequence(PositiveNumbersCollection::createFromIntegers($this->sequence));
    }
}