<?php
/**
 * User: michal_lap
 * Date: 25.11.18
 * Email: michalgaj@onet.pl
 */

namespace App\UI\DTO;


use App\Common\Types\PositiveInteger;
use App\Domain\Tree\Service\TreeParametersInterface;
use Swagger\Annotations as SWG;
use Symfony\Component\Validator\Constraints as Assert;

class TreeParameters implements TreeParametersInterface
{
    /**
     * Levels of tree. 1 means one root node with its children. 2 means one root with children of children, and so on.
     * @var integer
     *
     * @Assert\NotBlank()
     * @Assert\GreaterThanOrEqual(0)
     */
    private $treeDepth;

    /**
     * Defines how many children has single node. Eg. value 3 will produce nodes with 3 children each.
     * @var integer
     *
     * @Assert\NotBlank()
     * @Assert\GreaterThanOrEqual(0)
     */
    private $treeBranchSize;

    /**
     * @return PositiveInteger
     */
    public function getTreeDepth(): PositiveInteger
    {
        return new PositiveInteger($this->treeDepth);
    }

    /**
     * @param int $treeDepth
     */
    public function setTreeDepth(int $treeDepth): void
    {
        $this->treeDepth = $treeDepth;
    }

    /**
     * @return PositiveInteger
     */
    public function getTreeBranchSize(): PositiveInteger
    {
        return new PositiveInteger($this->treeBranchSize);
    }

    /**
     * @param int $treeBranchSize
     */
    public function setTreeBranchSize(int $treeBranchSize): void
    {
        $this->treeBranchSize = $treeBranchSize;
    }
}