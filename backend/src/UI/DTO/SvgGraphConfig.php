<?php
/**
 * User: michal_lap
 * Date: 25.11.18
 * Email: michalgaj@onet.pl
 */

namespace App\UI\DTO;


use App\Domain\Exception\InvalidConfigurationData;
use App\Domain\Graph\Service\SvgConfigInterface;
use Swagger\Annotations as SWG;
use Symfony\Component\Validator\Constraints as Assert;


class SvgGraphConfig implements SvgConfigInterface
{
    /**
     * Height(px) of single node
     * @var float
     *
     *
     * @Assert\GreaterThan(0)
     * @Assert\NotBlank()
     */
    private $nodeHeight = 10.00;

    /**
     * Width(px) of single node
     * @var float
     *
     * @Assert\GreaterThan(0)
     * @Assert\NotBlank()
     */
    private $nodeWidth = 10.00;

    /**
     * Size(px) of horizontal gap between nodes
     * @var float
     *
     * @Assert\GreaterThan(0)
     * @Assert\NotBlank()
     */
    private $gapHorizontalSize = 5.00;

    /**
     * Size(px) of vertical gap between levels of tree
     * @var float
     *
     * @Assert\GreaterThan(0)
     * @Assert\NotBlank()
     */
    private $gapVerticalSize = 5.00;

    /**
     * @param float $nodeHeight
     */
    public function setNodeHeight(float $nodeHeight): void
    {
        $this->nodeHeight = $nodeHeight;
    }

    /**
     * @param float $nodeWidth
     */
    public function setNodeWidth(float $nodeWidth): void
    {
        $this->nodeWidth = $nodeWidth;
    }

    /**
     * @param float $gapHorizontalSize
     */
    public function setGapHorizontalSize(float $gapHorizontalSize): void
    {
        $this->gapHorizontalSize = $gapHorizontalSize;
    }

    /**
     * @param float $gapVerticalSize
     */
    public function setGapVerticalSize(float $gapVerticalSize): void
    {
        $this->gapVerticalSize = $gapVerticalSize;
    }

    /**
     * @return float
     */
    public function getNodeHeight(): float
    {
        return $this->nodeHeight;
    }

    /**
     * @return float
     */
    public function getNodeWidth(): float
    {
        return $this->nodeWidth;
    }

    /**
     * @return float
     */
    public function getGapHorizontalSize(): float
    {
        return $this->gapHorizontalSize;
    }

    /**
     * @return float
     */
    public function getGapVerticalSize(): float
    {
        return $this->gapVerticalSize;
    }

    /**
     * @param array $configuration
     * @return Svg\SvgConfig
     * @throws InvalidConfigurationData
     */
    public static function createSvgConfig(array $configuration): Svg\SvgConfig
    {
        $config = new self();

        try {
            if (array_key_exists('nodeHeight', $configuration)) {
                $config->setNodeHeight(
                    new PositiveNumber((float)$configuration['nodeHeight'])
                );
            }

            if (array_key_exists('nodeWidth', $configuration)) {
                $config->setNodeWidth(
                    new PositiveNumber((float)$configuration['nodeWidth'])
                );
            }

            if (array_key_exists('gapHorizontalSize', $configuration)) {
                $config->setGapHorizontalSize(
                    new PositiveNumber((float)$configuration['gapHorizontalSize'])
                );
            }

            if (array_key_exists('gapVerticalSize', $configuration)) {
                $config->setGapVerticalSize(
                    new PositiveNumber((float)$configuration['gapVerticalSize'])
                );
            }
        } catch (\InvalidArgumentException $exception) {
            throw new InvalidConfigurationData('Provided configuration data is invalid');        }

        return $config;
    }
}