<?php
/**
 * User: michal_lap
 * Date: 28.11.18
 * Email: michalgaj@onet.pl
 */

namespace App\UI\DTO;



use App\Common\Collection\AbstractCollection;
use Symfony\Component\Validator\ConstraintViolationInterface;
use Symfony\Component\Validator\ConstraintViolationListInterface;

/**
 * @method ConstraintViolationInterface[] offsetGet($offset)
 */
class Errors extends AbstractCollection implements \JsonSerializable
{
    protected function getType(): string
    {
        return ConstraintViolationInterface::class;
    }

    public function jsonSerialize(): array
    {
        $errors = [];

        /** @var ConstraintViolationInterface $error */
        foreach ($this as $error) {
            $errors[$error->getPropertyPath()][] = $error->getMessage();
        }

        return [
            'errors' => $errors
        ];
    }

    public static function createFromViolationsList(ConstraintViolationListInterface $violations): self
    {
        $self = new self;
        foreach ($violations as $error) {
            $self[] = $error;
        }

        return $self;
    }
}