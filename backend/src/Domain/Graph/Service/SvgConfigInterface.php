<?php
/**
 * User: Michal Gaj
 * Date: 02.11.18
 * E-mail: michalgaj@onet.pl
 */

namespace App\Domain\Graph\Service;


interface SvgConfigInterface
{
    public function getNodeHeight(): float;
    public function getNodeWidth(): float;
    public function getGapHorizontalSize(): float;
    public function getGapVerticalSize(): float;
}