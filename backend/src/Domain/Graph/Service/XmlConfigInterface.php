<?php
/**
 * User: Michal Gaj
 * Date: 02.11.18
 * E-mail: michalgaj@onet.pl
 */

namespace App\Domain\Graph\Service;


interface XmlConfigInterface
{
    public function getChildTagName(): string;
}