<?php
/**
 * User: Michal Gaj
 * Date: 27.10.18
 * E-mail: michalgaj@onet.pl
 */

namespace App\Domain\Graph\Service;


use App\Domain\Graph\GraphInterface;
use App\Domain\Tree\Tree;
use App\Domain\Graph\Xml;
use App\Domain\Graph\Svg;

class GraphService
{
    public function generateXmlGraph(Tree $tree, XmlConfigInterface $config): GraphInterface
    {
        return new Xml\Graph($tree, $config);
    }

    public function generateSvgGraph(Tree $tree, SvgConfigInterface $config): GraphInterface
    {
        return new Svg\Graph($tree, $config);
    }
}