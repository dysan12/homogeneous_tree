<?php
/**
 * User: Michal Gaj
 * Date: 27.10.18
 * E-mail: michalgaj@onet.pl
 */

namespace App\Domain\Graph;


interface GraphInterface
{
    public function getRepresentation(): string;
}