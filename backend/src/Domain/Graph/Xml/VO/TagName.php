<?php
/**
 * User: Michal Gaj
 * Date: 02.11.18
 * E-mail: michalgaj@onet.pl
 */

namespace App\Domain\Graph\Xml\VO;


class TagName
{
    /**
     * @var string
     */
    private $tagName;

    public function __construct(string $tagName)
    {
        if (str_split($tagName, 3)[0] === 'xml') {
            throw new \InvalidArgumentException('TagName cannot be started by "xml" word');
        }

        if (ctype_digit($tagName[0])) {
            throw new \InvalidArgumentException('TagName cannot be started by digit');
        }

        if ($tagName[0] === '.') {
            throw new \InvalidArgumentException('TagName cannot be started by dot sign');
        }

        $this->tagName = $tagName;
    }

    /**
     * @return string
     */
    public function getTagName(): string
    {
        return $this->tagName;
    }
}