<?php
/**
 * User: Michal Gaj
 * Date: 02.11.18
 * E-mail: michalgaj@onet.pl
 */

namespace App\Domain\Graph\Xml;


use App\Domain\Graph\Service\XmlConfigInterface;
use App\Domain\Graph\Xml\VO\TagName;

class XmlConfig implements XmlConfigInterface
{
    /** @var TagName  */
    private $tagName;

    public function __construct()
    {
        $this->tagName = new TagName('node');
    }

    /**
     * @return string
     */
    public function getChildTagName(): string
    {
        return $this->tagName->getTagName();
    }

    /**
     * @param TagName $tagName
     */
    public function setChildTagName(TagName $tagName): void
    {
        $this->tagName = $tagName;
    }
}