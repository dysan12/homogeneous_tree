<?php
/**
 * User: Michal Gaj
 * Date: 27.10.18
 * E-mail: michalgaj@onet.pl
 */

namespace App\Domain\Graph\Xml;


use App\Domain\Graph\GraphInterface;
use App\Domain\Graph\Service\XmlConfigInterface;
use App\Domain\Tree\Node\ChildrenableInterface;
use App\Domain\Tree\Node\OrderableInterface;
use App\Domain\Tree\Tree;

class Graph implements GraphInterface
{
    private $xml;
    private $tree;
    /**
     * @var XmlConfigInterface
     */
    private $config;

    public function __construct(Tree $tree, XmlConfigInterface $config)
    {
        $this->tree = $tree;
        $this->config = $config;
    }

    public function getRepresentation(): string
    {
        if (null === $this->xml) {
            $this->xml = new \SimpleXMLElement('<tree></tree>', 2);

            $this->processTreeToXml($this->xml, $this->tree);
        }

        return $this->xml->asXML();
    }

    private function processTreeToXml(\SimpleXMLElement $xmlTree, Tree $tree): void
    {
        $root = $tree->getRoot();

        $xmlRoot = $xmlTree->addChild('root');
        $xmlRoot->addAttribute('no', $root->getNo());

        $this->processNodeChildren($root->getChildren(), $xmlRoot);
    }

    private function processNodeChildren(array $children, \SimpleXMLElement $xmlNode): void
    {
        foreach ($children as $child) {
            $xmlChild = $xmlNode->addChild($this->config->getChildTagName());
            if ($child instanceof OrderableInterface) {
                $xmlChild->addAttribute('no', $child->getNo());
            }

            if ($child instanceof ChildrenableInterface) {
                $this->processNodeChildren($child->getChildren(), $xmlChild);
            }
        }
    }
}