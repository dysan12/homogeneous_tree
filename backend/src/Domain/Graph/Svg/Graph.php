<?php
/**
 * User: Michal Gaj
 * Date: 28.10.18
 * E-mail: michalgaj@onet.pl
 */

namespace App\Domain\Graph\Svg;


use App\Domain\Graph\GraphInterface;
use App\Domain\Graph\Service\SvgConfigInterface;
use App\Domain\Graph\Svg\Positioning\Range;
use App\Domain\Tree\Node\ChildrenableInterface;
use App\Domain\Tree\Node\OrderableInterface;
use App\Domain\Tree\Tree;

class Graph implements GraphInterface
{
    // TODO consider moving it to const array
    private const LABEL_STACK_INDEX = 3;
    private const NODES_STACK_INDEX = 2;
    private const LINES_STACK_INDEX = 0;

    /** @var \SimpleXMLElement|null */
    private $svgXml;
    /** @var Tree  */
    private $tree;
    /** @var SvgConfigInterface  */
    private $config;
    /** @var Stack */
    private $renderingStack;

    public function __construct(Tree $tree, SvgConfigInterface $config)
    {
        $this->tree = $tree;
        $this->config = $config;

        $this->renderingStack = new Stack();
    }

    public function getRepresentation(): string
    {
        $parentMarkup = sprintf(
            '<svg width="%d" height="%d"></svg>',
            $this->getImageRequiredWidth(), $this->getImageRequiredHeight()
        );

        if (null === $this->svgXml) {
            $this->svgXml = new \SimpleXMLElement($parentMarkup);

            $this->processTreeToSvg($this->svgXml, $this->tree);
        }

        foreach ($this->renderingStack as $stack) {
            foreach ($stack as $element) {
                if ($element instanceof \SimpleXMLElement) {
                    $node = $this->svgXml->addChild($element->getName(), (string)$element);
                    foreach ($element->attributes() as $attribute => $value) {
                        $node->addAttribute($attribute, $value);
                        // TODO its ugly, refactor this
                    }
                }
            }
        }

        return $this->svgXml->asXML();
    }

    private function processTreeToSvg(\SimpleXMLElement $svgTree, Tree $tree): void
    {
        $yRange = new Range(0, $this->getImageRequiredHeight(), $this->tree->getDepth() + 1);
        $xRange = new Range(0, $this->getImageRequiredWidth(), $this->tree->getBranchSize());

        $root = $tree->getRoot();

        $svgRoot = $this->createNode(
            $xRange->getMiddlePoint(),
            $yRange->getCurrentSubRange()->getMiddlePoint(),
            $this->config->getNodeWidth() / 2,
            $this->config->getNodeHeight() / 2
        );
        $svgRoot->addAttribute('fill', 'yellow');

        $this->renderingStack->add(self::NODES_STACK_INDEX, $svgRoot);

        $this->processNodeChildren($root->getChildren(), $xRange, $yRange);
    }

    private function processNodeChildren(array $children, Range $xRange, Range $yRange): void
    {
        $lineXStart = $xRange->getMiddlePoint();
        $lineYStart = $yRange->getCurrentSubRange()->getMiddlePoint();

        $branchRange = $yRange->getNextSubRange();
        foreach ($children as $child) {
            $childXRange = $xRange->getCurrentSubRange();

            $svgChild = $this->createNode(
                $childXRange->getMiddlePoint(),
                $branchRange->getMiddlePoint(),
                $this->config->getNodeWidth() / 2,
                $this->config->getNodeHeight() / 2
            );
            if ($child instanceof OrderableInterface) {
                $svgChild->addAttribute('no', $child->getNo());

                if ($this->config->getNodeHeight() >= $this->config->getNodeWidth()) {
                    $textSize = $this->config->getNodeWidth() / 2;
                } else {
                    $textSize = $this->config->getNodeHeight() / 2;
                }
                $label = $this->createLabel(
                    $childXRange->getMiddlePoint(),
                    $branchRange->getMiddlePoint(),
                    (string)$child->getNo(),
                    $textSize
                );
                $label->addAttribute('fill', 'skyblue');

                $this->renderingStack->add(self::LABEL_STACK_INDEX, $label);
            }
            $this->renderingStack->add(self::NODES_STACK_INDEX, $svgChild);

            $lineXEnd = $childXRange->getMiddlePoint();
            $lineYEnd = $branchRange->getMiddlePoint();
            $line = $this->createLine($lineXStart, $lineYStart, $lineXEnd, $lineYEnd);
            $this->renderingStack->add(self::LINES_STACK_INDEX, $line);

            if ($child instanceof ChildrenableInterface) {
                $this->processNodeChildren($child->getChildren(), $childXRange, clone $yRange);
            }

            $xRange->getNextSubRange();
        }
    }

    private function createLine(float $xStart, float $yStart, float $xEnd, float $yEnd): \SimpleXMLElement
    {
        $svgLine = new \SimpleXMLElement('<line></line>');
        $svgLine->addAttribute('x1', $xStart);
        $svgLine->addAttribute('y1', $yStart);
        $svgLine->addAttribute('x2', $xEnd);
        $svgLine->addAttribute('y2', $yEnd);

        $svgLine->addAttribute('style', "stroke:red;stroke-width:2");

        return $svgLine;
    }

    private function createNode(float $xPos, float $yPos, float $xRad, float $yRad): \SimpleXMLElement
    {
        $svgNode = new \SimpleXMLElement('<ellipse></ellipse>');
        $svgNode->addAttribute('cx', $xPos);
        $svgNode->addAttribute('cy', $yPos);
        $svgNode->addAttribute('rx', $xRad);
        $svgNode->addAttribute('ry', $yRad);

        return $svgNode;
    }

    private function createLabel(float $xPos, float $yPos, string $text, float $fontSize): \SimpleXMLElement
    {
        $svgNode = new \SimpleXMLElement(sprintf('<text>%s</text>', $text));
        $fontWidth = strlen($text) === 1 ? $fontSize / 2 : (strlen($text) === 2 ? $fontSize : $fontSize * 1.5);
        $svgNode->addAttribute('textLength', $fontWidth);
        $svgNode->addAttribute('lengthAdjust', 'spacingAndGlyphs');

        $svgNode->addAttribute('x', $xPos);
        $svgNode->addAttribute('y', $yPos );
        $svgNode->addAttribute('alignment-baseline', 'central' );//+ ($fontSize / 4));
        $svgNode->addAttribute('text-anchor', 'middle' );//+ ($fontSize / 4));
        $svgNode->addAttribute('style', implode(';', [
            sprintf('font-size: %d', $fontSize),
            'font-family: FreeMono, sans-serif'
        ]));

        return $svgNode;
    }


    private function getImageRequiredWidth(): int
    {
        $maxChildrenQuantity = pow($this->tree->getBranchSize(), $this->tree->getDepth());

        $nodesWidth = $maxChildrenQuantity * $this->config->getNodeWidth();
        $gapsWidth = ($maxChildrenQuantity - 1) * $this->config->getGapHorizontalSize();

        return  $nodesWidth + $gapsWidth;
    }

    private function getImageRequiredHeight(): int
    {
        $levelsQuantity = $this->tree->getDepth() + 1;

        $nodesHeight = $levelsQuantity * $this->config->getNodeHeight();
        $gapsHeight = ($levelsQuantity - 1) * $this->config->getGapVerticalSize();

        return $nodesHeight + $gapsHeight;
    }
}
