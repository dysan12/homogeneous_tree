<?php
/**
 * User: Michal Gaj
 * Date: 05.11.18
 * E-mail: michalgaj@onet.pl
 */

namespace App\Domain\Graph\Svg;

class Stack implements \Iterator
{
    private $stack = [];

    public function get(int $index): array
    {
        return $this->stack[$index] ?? [];
    }

    public function add(int $index, $element)
    {
        if (false === key_exists($index, $this->stack) || false === is_array($this->stack[$index])) {
            $this->stack[$index] = [];
            ksort($this->stack);
        }

        $this->stack[$index][] = $element;
    }

    public function current()
    {
        return current($this->stack);
    }

    public function next(): void
    {
        next($this->stack);
    }

    public function key()
    {
        return key($this->stack);
    }

    public function valid()
    {
        return key($this->stack) !== null;
    }

    public function rewind()
    {
        reset($this->stack);
    }
}