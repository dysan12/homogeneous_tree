<?php
/**
 * User: Michal Gaj
 * Date: 02.11.18
 * E-mail: michalgaj@onet.pl
 */

namespace App\Domain\Graph\Svg;


use App\Common\Types\PositiveNumber;
use App\Domain\Graph\Service\SvgConfigInterface;

class SvgConfig implements SvgConfigInterface
{
    /** @var PositiveNumber */
    private $nodeWidth;
    /** @var PositiveNumber */
    private $nodeHeight;
    /** @var PositiveNumber */
    private $gapVerticalSize;
    /** @var PositiveNumber */
    private $gapHorizontalSize;

    public function __construct()
    {
        $this->nodeWidth = new PositiveNumber(20);
        $this->nodeHeight = new PositiveNumber(20);
        $this->gapHorizontalSize = new PositiveNumber(7);
        $this->gapVerticalSize = new PositiveNumber(10);
    }

    /**
     * @return float
     */
    public function getNodeWidth(): float
    {
        return $this->nodeWidth->getNumber();
    }

    /**
     * @param PositiveNumber $nodeWidth
     */
    public function setNodeWidth(PositiveNumber $nodeWidth): void
    {
        $this->nodeWidth = $nodeWidth;
    }

    /**
     * @return float
     */
    public function getNodeHeight(): float
    {
        return $this->nodeHeight->getNumber();
    }

    /**
     * @param PositiveNumber $nodeHeight
     */
    public function setNodeHeight(PositiveNumber $nodeHeight): void
    {
        $this->nodeHeight = $nodeHeight;
    }

    /**
     * @return float
     */
    public function getGapVerticalSize(): float
    {
        return $this->gapVerticalSize->getNumber();
    }

    /**
     * @param PositiveNumber $gapVerticalSize
     */
    public function setGapVerticalSize(PositiveNumber $gapVerticalSize): void
    {
        $this->gapVerticalSize = $gapVerticalSize;
    }

    /**
     * @return float
     */
    public function getGapHorizontalSize(): float
    {
        return $this->gapHorizontalSize->getNumber();
    }

    /**
     * @param PositiveNumber $gapHorizontalSize
     */
    public function setGapHorizontalSize(PositiveNumber $gapHorizontalSize): void
    {
        $this->gapHorizontalSize = $gapHorizontalSize;
    }
}