<?php
/**
 * User: Michal Gaj
 * Date: 28.10.18
 * E-mail: michalgaj@onet.pl
 */

namespace App\Domain\Graph\Svg\Positioning;


class Range
{
    /**
     * @var float
     */
    private $start;
    /**
     * @var float
     */
    private $end;
    /**
     * @var int
     */
    private $division;

    /**
     * @var int
     */
    private $counter;

    /** @var self */
    private $currentSubRange;

    /**
     * Range constructor.
     * @param float $start
     * @param float $end
     * @param int $division
     * @throws \InvalidArgumentException
     */
    public function __construct(float $start, float $end, int $division)
    {
        if ($end <= $start) {
            throw new \InvalidArgumentException('Range\'s end cannot be lower nor equal to start');
        }
        if ($division <= 0) {
            throw new \InvalidArgumentException('Division cannot be neither lower nor equal to zero');
        }

        $this->start = $start;
        $this->end = $end;
        $this->division = $division;

        $this->counter = 0;
    }

    public function getMiddlePoint(): float
    {
        return ($this->end + $this->start) / 2;
    }

    /**
     * @return float
     */
    public function getStart(): float
    {
        return $this->start;
    }

    /**
     * @return float
     */
    public function getEnd(): float
    {
        return $this->end;
    }

    /**
     * @return Range
     * @throws \OutOfBoundsException
     */
    public function getNextSubRange(): self
    {
        if ($this->hasNextSubRange()) {
            $this->counter++;
            $nextSubRange = $this->getSubRange();
            $this->currentSubRange = $nextSubRange;
        } else {
            throw new \OutOfBoundsException('Cannot receive next range. Rewinding required.');
        }

        return $nextSubRange;
    }

    public function getCurrentSubRange()
    {
        if (null === $this->currentSubRange) {
            $this->currentSubRange = $this->getSubRange();
        }

        return $this->currentSubRange;
    }

    private function getSubRange(): self
    {
        $range = $this->end - $this->start;
        $rangePart = $range / $this->division;
        $rangeStart = $this->start + ($rangePart * $this->counter);
        $rangeEnd = $rangeStart + $rangePart;

        return new self($rangeStart, $rangeEnd, $this->division);
    }

    public function hasNextSubRange(): bool
    {
        return $this->counter < $this->division;
    }
}