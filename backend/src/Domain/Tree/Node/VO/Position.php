<?php
/**
 * User: michal_lap
 * Date: 24.11.18
 * Email: michalgaj@onet.pl
 */

namespace App\Domain\Tree\Node\VO;



use App\Common\Types\PositiveInteger;
use App\Common\Types\PositiveIntegerZeroable;

class Position
{
    /**
     * @var PositiveIntegerZeroable
     */
    private $level;
    /**
     * @var PositiveInteger
     */
    private $nodeNo;

    public function __construct(PositiveIntegerZeroable $level, PositiveInteger $nodeNo)
    {
        $this->level = $level;
        $this->nodeNo = $nodeNo;
    }

    /**
     * @return PositiveIntegerZeroable
     */
    public function getLevel(): PositiveIntegerZeroable
    {
        return $this->level;
    }

    /**
     * @return PositiveInteger
     */
    public function getNodeNo(): PositiveInteger
    {
        return $this->nodeNo;
    }

    /**
     * @param int $level
     * @param int $nodeNo
     * @return Position
     * @throws \InvalidArgumentException
     */
    public static function createFromInts(int $level, int $nodeNo): self
    {
        return new self(new PositiveIntegerZeroable($level), new PositiveInteger($nodeNo));
    }
}