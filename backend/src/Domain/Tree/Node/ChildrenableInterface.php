<?php
/**
 * User: Michal Gaj
 * Date: 26.10.18
 * E-mail: michalgaj@onet.pl
 */

namespace App\Domain\Tree\Node;


interface ChildrenableInterface
{
    public function getChildren(): array;

    /**
     * @param array $children
     * @throws \InvalidArgumentException
     */
    public function replaceChildren(array $children): void;
}