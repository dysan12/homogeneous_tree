<?php
/**
 * User: Michal Gaj
 * Date: 26.10.18
 * E-mail: michalgaj@onet.pl
 */

namespace App\Domain\Tree\Node;


class Root implements OrderableInterface, ChildrenableInterface
{
    private $no = 1;

    private $children = [];

    public function __construct(int $depth, int $childrenQuantity, Generator $nodeGenerator)
    {
        if ($depth > 0) {
            $childrenDepth = $depth - 1;
            if ($depth === 1) {
                $this->children = $nodeGenerator->generateLeafs($childrenQuantity);
            } else {
                $this->children = $nodeGenerator->generateInternals($childrenDepth, $childrenQuantity);
            }
        }
    }

    public function getNo(): int
    {
        return $this->no;
    }

    public function getChildren(): array
    {
        return $this->children;
    }

    /**
     * @param array $children
     * @throws \InvalidArgumentException
     */
    public function replaceChildren(array $children): void
    {
        if (\count($children) === \count($this->children)) {
            $this->children = $children;
        } else {
            throw new \InvalidArgumentException(
                'Replacing children set must contains the same number of child'
            );
        }
    }
}