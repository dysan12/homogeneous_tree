<?php
/**
 * User: michal_lap
 * Date: 25.11.18
 * Email: michalgaj@onet.pl
 */

namespace App\Domain\Tree\Node;


interface NodeInterface extends ChildrenableInterface, OrderableInterface
{

}