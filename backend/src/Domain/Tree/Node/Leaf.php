<?php
/**
 * User: Michal Gaj
 * Date: 26.10.18
 * E-mail: michalgaj@onet.pl
 */

namespace App\Domain\Tree\Node;


class Leaf implements OrderableInterface
{
    private $no;

    public function __construct(int $no)
    {
        $this->no = $no;
    }

    public function getNo(): int
    {
        return $this->no;
    }
}