<?php
/**
 * User: Michal Gaj
 * Date: 26.10.18
 * E-mail: michalgaj@onet.pl
 */

namespace App\Domain\Tree\Node;


use App\Domain\Tree\Node\Generator;

class Internal implements OrderableInterface, ChildrenableInterface
{
    private $no;

    private $children = [];

    public function __construct(int $no, int $depth, int $childrenQuantity, Generator $nodeGenerator)
    {
        $this->no = $no;
        if ($depth > 0) {
            $childrenDepth = $depth - 1;
            if ($depth === 1) {
                $this->children = $nodeGenerator->generateLeafs($childrenQuantity);
            } else {
                $this->children = $nodeGenerator->generateInternals($childrenDepth, $childrenQuantity);
            }
        }
    }

    public function getChildren(): array
    {
        return $this->children;
    }

    public function getNo(): int
    {
        return $this->no;
    }

    /**
     * @param array $children
     * @throws \InvalidArgumentException
     */
    public function replaceChildren(array $children): void
    {
        if (count($children) === count($this->children)) {
            $this->children = $children;
        } else {
            throw new \InvalidArgumentException(
                'Replacing children set must contains the same number of child'
            );
        }
    }
}