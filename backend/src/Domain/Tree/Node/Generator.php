<?php
/**
 * User: Michal Gaj
 * Date: 26.10.18
 * E-mail: michalgaj@onet.pl
 */

namespace App\Domain\Tree\Node;


use App\Domain\Tree\Node\Internal;
use App\Domain\Tree\Node\Leaf;

class Generator
{
    /**
     * @param int $childrenQuantity
     * @return array|Leaf[]
     */
    public function generateLeafs(int $childrenQuantity): array
    {
        $leafs = [];
        for ($i = 1; $i <= $childrenQuantity; $i++) {
            $leafs[] = new Leaf($i);
        }

        return $leafs;
    }

    /**
     * @param int $depth
     * @param int $childrenQuantity
     * @return array|Internal[]
     */
    public function generateInternals(int $depth, int $childrenQuantity): array
    {
        $internals = [];
        for ($i = 1; $i <= $childrenQuantity; $i++) {
            $internals[] = new Internal($i, $depth, $childrenQuantity, $this);
        }

        return $internals;
    }
}