<?php
/**
 * User: michal_lap
 * Date: 25.11.18
 * Email: michalgaj@onet.pl
 */

namespace App\Domain\Tree\Service;



use App\Common\Types\PositiveInteger;

interface TreeParametersInterface
{
    public function getTreeDepth(): PositiveInteger;
    public function getTreeBranchSize(): PositiveInteger;
}