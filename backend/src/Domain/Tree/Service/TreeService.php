<?php
/**
 * User: Michal Gaj
 * Date: 21.10.18
 */

namespace App\Domain\Tree\Service;


use App\Domain\Exception;
use App\Domain\Tree\Node;
use App\Domain\Tree\Tree;
use App\Domain\Tree\Command;

class TreeService
{
    /**
     * @var Node\Generator
     */
    private $nodeGenerator;
    /**
     * @var Command\Handler
     */
    private $commandHandler;

    public function __construct(Node\Generator $nodeGenerator, Command\Handler $commandHandler)
    {
        $this->nodeGenerator = $nodeGenerator;
        $this->commandHandler = $commandHandler;
    }

    /**
     * @param TreeParametersInterface $treeParameters
     * @param array $commands
     * @return Tree
     * @throws Exception\CommandExecution
     * @throws Exception\CommandNotSupported
     */
    public function createTree(TreeParametersInterface $treeParameters, array $commands = []): Tree
    {
        $tree = new Tree($treeParameters, $this->nodeGenerator);

        foreach ($commands as $command) {
            $this->commandHandler->handle($command, $tree);
        }

        return $tree;
    }
}