<?php
/**
 * User: michal_lap
 * Date: 25.11.18
 * Email: michalgaj@onet.pl
 */

namespace App\Domain\Tree\Service\Command;


use App\Domain\Tree\Node\VO\Position;
use App\Domain\Tree\Operation\Swap\Sequence;

interface SwapInterface
{
    public function getPosition(): Position;

    public function getSequence(): Sequence;
}