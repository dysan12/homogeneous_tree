<?php
/**
 * User: Michal Gaj
 * Date: 21.10.18
 */

namespace App\Domain\Tree;


use App\Domain\Exception\SonPositionException;
use App\Domain\Tree\Node\Root;
use App\Domain\Tree\Node;
use App\Domain\Tree\Service\TreeParametersInterface;

class Tree
{
    /** @var Root  */
    private $root;
    private $branchSize;
    private $depth;

    public function __construct(TreeParametersInterface $treeParameters, Node\Generator $nodeGenerator)
    {
        $this->depth = $treeParameters->getTreeDepth();
        $this->branchSize = $treeParameters->getTreeBranchSize();
        $this->root = new Root(
            $treeParameters->getTreeDepth()->getNumber(),
            $treeParameters->getTreeBranchSize()->getNumber(),
            $nodeGenerator
        );
    }

    public function getRoot(): Root
    {
        return $this->root;
    }

    /**
     * @return int
     */
    public function getBranchSize(): int
    {
        return $this->branchSize->getNumber();
    }

    /**
     * @return int
     */
    public function getDepth(): int
    {
        return $this->depth->getNumber();
    }

    /**
     * @param Node\VO\Position $childrenPosition
     * @return mixed
     * @throws SonPositionException
     */
    public function getSon(Node\VO\Position $childrenPosition)
    {
        return $this->getSonByPosition($this->getRoot(), $childrenPosition);
    }

    /**
     * @param Node\ChildrenableInterface $node
     * @param Node\VO\Position $childrenPosition
     * @param int $currentLevel
     * @param int $indexingShift
     * @return mixed
     * @throws SonPositionException
     */
    private function getSonByPosition(Node\ChildrenableInterface $node, Node\VO\Position $childrenPosition, int $currentLevel = 0, int $indexingShift = 0)
    {
        // in case of requesting for root node
        if ($childrenPosition->getLevel()->getNumber() === 0 && $childrenPosition->getNodeNo()->getNumber() === 1) {
            return $node;
        }
        $reachedRequiredLevel = $childrenPosition->getLevel()->getNumber() - 1 === $currentLevel;
        if ($reachedRequiredLevel) {
            /**
             * -1 because of difference of indexing array and Nodes -> [[1,2,3], [4,5,6], [7,8,9]], 6 is placed in [1]
             */
            $index = ($childrenPosition->getNodeNo()->getNumber() - 1) % $this->getBranchSize();
            $son = $node->getChildren()[$index];
        } else {
            $nodesInChildLevel = $this->getBranchSize() ** ($childrenPosition->getLevel()->getNumber() - ($currentLevel));

            /**
             * Child index in where is placed required children position
             */
            $childIndex = (int)(($childrenPosition->getNodeNo()->getNumber() - $indexingShift - 1) / ($nodesInChildLevel / $this->getBranchSize()));
            foreach ($node->getChildren() as $key => $child) {
                if ($childIndex === $key && $child instanceof Node\ChildrenableInterface) {
                    $son = $this->getSonByPosition(
                        $child, $childrenPosition, $currentLevel + 1, ($childIndex * $nodesInChildLevel / $this->getBranchSize()) + $indexingShift
                    );
                }
            }
        }

        if (false === isset($son)) {
            throw new SonPositionException('Son on requested position doesn\'t exist!');
        }

        return $son;
    }
}