<?php
/**
 * User: michal_lap
 * Date: 24.11.18
 * Email: michalgaj@onet.pl
 */

namespace App\Domain\Tree\Command;


use App\Domain\Exception;
use App\Domain\Tree\Node\ChildrenableInterface;
use App\Domain\Tree\Node\VO\Position;
use App\Domain\Tree\Operation\Swap\Sequence;
use App\Domain\Tree\Operation\Swap\Swapper;
use App\Domain\Tree\Service\Command;
use App\Domain\Tree\Tree;

class Handler
{
    /**
     * @var Swapper
     */
    private $swapper;

    public function __construct(Swapper $swapper)
    {
        $this->swapper = $swapper;
    }

    /**
     * @param $command
     * @param Tree $tree
     * @throws Exception\CommandNotSupported
     * @throws Exception\CommandExecution
     */
    public function handle($command, Tree $tree): void
    {
        $commandClass = \get_class($command);

        if ($command instanceof Command\SwapInterface) {
            try {
                /** @var Command\SwapInterface $command */
                $this->swap($tree, $command->getPosition(), $command->getSequence());
            } catch (\Exception $exception) {
                throw new Exception\CommandExecution(
                    sprintf('Cannot execute command: "%s"', $commandClass), 0, $exception
                );
            }

        } else {
            throw new Exception\CommandNotSupported(sprintf('Command "%d" is not supported', \get_class($command)));
        }
    }

    public function swap(Tree $tree, Position $nodePosition, Sequence $sequence): void
    {
        $node = $tree->getSon($nodePosition);

        if ($node instanceof ChildrenableInterface) {
            $this->swapper->swap($node, $sequence);
        }
    }
}