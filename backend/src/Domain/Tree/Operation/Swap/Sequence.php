<?php
/**
 * User: michal_lap
 * Date: 21.11.18
 * Email: michalgaj@onet.pl
 */

namespace App\Domain\Tree\Operation\Swap;


use App\Common\Collection\PositiveNumbersCollection;
use App\Common\Types\PositiveNumber;

class Sequence
{
    /**
     * @var PositiveNumbersCollection
     */
    private $sequences;

    public function __construct(PositiveNumbersCollection $sequences)
    {
        $this->sequences = $sequences;
    }

    /**
     * @return PositiveNumbersCollection|PositiveNumber[]
     */
    public function getSequence(): PositiveNumbersCollection
    {
        return $this->sequences;
    }

    /**
     * @param string $strSequence
     * @param string $delimiter
     * @return Sequence
     * @throws \InvalidArgumentException - when $strSequence contains invalid elements
     */
    public static function createFromString(string $strSequence, string $delimiter): self
    {
        $positiveNumbers = new PositiveNumbersCollection();
        $sequence = explode($delimiter, $strSequence);
        foreach ($sequence as $number) {
            if (false === is_numeric($number)) {
                throw new \InvalidArgumentException('Elements of $strSequence must be of numeric type');
            }

            $positiveNumbers[] = new PositiveNumber((float)$number);
        }

        return new self($positiveNumbers);
    }
}