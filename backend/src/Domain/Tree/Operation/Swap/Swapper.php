<?php
/**
 * User: michal_lap
 * Date: 21.11.18
 * Email: michalgaj@onet.pl
 */

namespace App\Domain\Tree\Operation\Swap;


use App\Domain\Exception\SequenceSwappingException;
use App\Domain\Tree\Node\ChildrenableInterface;
use App\Domain\Tree\Node\OrderableInterface;

/**
 * Class Swapper - swapper for Childrenable elements. Each child of element need to be Orderable.
 * @package App\Domain\Tree\Operation\Swap
 */
class Swapper
{
    /**
     * Algorithm tries to resolve value of sequence number to any of node's children number.
     * Eg. if sequence number will be 33, and the greatest number of children will be 3, then 33
     * is resolved as 1(by computing 33 % 3 + 1)
     *
     * @param ChildrenableInterface $node - each child of node need to be Orderable
     * @param Sequence $sequence
     * @return ChildrenableInterface
     * @throws SequenceSwappingException - throws if alhorithm cannot resolve sequence number to
     *  any of children number
     */
    public function swap(ChildrenableInterface $node, Sequence $sequence): ChildrenableInterface
    {
        if (false === $this->hasNodeValidChildren($node)) {
            throw new \InvalidArgumentException(
                'All children of node must be Orderable in order to be swapped'
            );
        }

        $maxOrder = $this->getMaxOrder($node);
        $children = $node->getChildren();
        foreach ($sequence->getSequence() as $sequenceElement) {

            if ($maxOrder < $sequenceElement->getNumber()) {
                $comparisonNum = $sequenceElement->getNumber() % $maxOrder + 1;
            } else {
                $comparisonNum = $sequenceElement->getNumber();
            }
            /**
             * @var int $childIndex
             * @var OrderableInterface $child
             */
            foreach ($children as $childIndex => $child) {
                if ($child->getNo() == $comparisonNum) {
                    $current = $childIndex;
                    break;
                }
            }

            if (isset($current)) {
                if (isset($previous)) {
                    $temp = $children[$previous];
                    $children[$previous] = $children[$current];
                    $children[$current] = $temp;
                } else {
                    $previous = $current;
                }
                unset($current);
            } else {
                throw new SequenceSwappingException(
                    'Sequence number cannot be resolved to any of node children'
                );
            }
        }
        $node->replaceChildren($children);
        return $node;
    }

    private function hasNodeValidChildren(ChildrenableInterface $node): bool
    {
        $result = true;
        foreach ($node->getChildren() as $child) {
            if (false === ($child instanceof OrderableInterface)) {
                $result = false;
                break;
            }
        }

        return $result;
    }

    private function getMaxOrder(ChildrenableInterface $node): int
    {
        $max = 0;
        /** @var OrderableInterface $child */
        foreach ($node->getChildren() as $child) {
            if ($child->getNo() > $max) {
                $max = $child->getNo();
            }
        }

        return $max;
    }
}