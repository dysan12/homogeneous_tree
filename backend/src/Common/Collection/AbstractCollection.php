<?php
/**
 * User: michal_lap
 * Date: 21.11.18
 * Email: michalgaj@onet.pl
 */

namespace App\Common\Collection;

abstract class AbstractCollection implements \Iterator, \ArrayAccess, \Countable
{
    /** @var array */
    private $data = [];

    abstract protected function getType(): string;

    /**
     * AbstractCollection constructor.
     * @param array $data
     * @throws \InvalidArgumentException - if invalid $data provided
     */
    public function __construct(array $data = [])
    {
        foreach ($data as $item) {
            $this->offsetSet(null, $item);
        }
    }

    public function current()
    {
        return current($this->data);
    }

    public function next(): void
    {
        next($this->data);
    }

    public function key()
    {
        return key($this->data);
    }

    public function valid()
    {
        return $this->key() !== null;
    }

    public function rewind()
    {
        reset($this->data);
    }

    public function offsetExists($offset)
    {
        return isset($this->data[$offset]);
    }

    public function offsetGet($offset)
    {
        return $this->data[$offset];
    }

    public function offsetSet($offset, $value): void
    {
        $type = \gettype($value);
        $requiredType = $this->getType();

        if ($type === 'object') {
            $isValidType =
                $value instanceof $requiredType ||
                get_class($value) === $this->getType();
        } else {
            $isValidType = $type === $this->getType();
        }

        if ($isValidType) {
            if (null === $offset) {
                $this->data[] = $value;
            } else {
                $this->data[$offset] = $value;
            }
        } else {
            throw new \InvalidArgumentException('Elements in collection must be type of ' . $requiredType);
        }
    }

    public function offsetUnset($offset)
    {
        unset($this->data[$offset]);
    }

    public function count()
    {
        return count($this->data);
    }
}