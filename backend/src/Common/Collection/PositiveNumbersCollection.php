<?php
/**
 * User: michal_lap
 * Date: 21.11.18
 * Email: michalgaj@onet.pl
 */

namespace App\Common\Collection;



use App\Common\Types\PositiveNumber;

class PositiveNumbersCollection extends AbstractCollection
{
    protected function getType(): string
    {
        return PositiveNumber::class;
    }

    /**
     * @param array $integers
     * @return PositiveNumbersCollection
     * @throws \InvalidArgumentException
     */
    public static function createFromIntegers(array $integers): self
    {
        $collection = new self();

        foreach ($integers as $integer) {
            $collection[] = new PositiveNumber($integer);
        }

        return $collection;
    }
}