<?php
/**
 * User: Michal Gaj
 * Date: 02.11.18
 * E-mail: michalgaj@onet.pl
 */

namespace App\Common\Types;


class PositiveNumber
{
    /**
     * @var float
     */
    private $number;

    public function __construct(float $number)
    {
        if ($number <= 0) {
            throw new \InvalidArgumentException('Number must be positive!');
        }

        $this->number = $number;
    }

    /**
     * @return float
     */
    public function getNumber(): float
    {
        return $this->number;
    }
}