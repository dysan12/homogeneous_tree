<?php
/**
 * User: michal_lap
 * Date: 24.11.18
 * Email: michalgaj@onet.pl
 */

namespace App\Common\Types;


class PositiveIntegerZeroable
{
    /**
     * @var int
     */
    private $number;

    /**
     * PositiveInteger constructor.
     * @param int $number
     * @throws \InvalidArgumentException
     */
    public function __construct(int $number)
    {
        if ($number < 0) {
            throw new \InvalidArgumentException('Number must be positive!');
        }

        $this->number = $number;
    }

    /**
     * @return int
     */
    public function getNumber(): int
    {
        return $this->number;
    }
}