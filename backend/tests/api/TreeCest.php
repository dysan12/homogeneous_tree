<?php namespace App\Tests;

use App\Tests\ApiTester;
use Codeception\Util\HttpCode;
use Codeception\Util\Xml;

class TreeCest
{
    public function _before(ApiTester $I)
    {
        $I->haveHttpHeader('Content-Type', 'application/json');
    }

    public function requestSvgTree(ApiTester $I)
    {
        $I->wantTo('Request for SVG Tree');
        $json = [
            'treeParameters' => [
                'treeDepth' => 2,
                'treeBranchSize' => 2
            ],
            'config' => [
                'nodeHeight' => 40,
                'nodeWidth' => 40,
                'gapHorizontalSize' => 40,
                'gapVerticalSize' => 40,
            ],
            'commands' => [
                'swap' => [
                    ['level' => 0, 'nodeNo' => 1, 'sequence' => [1,3,4,5]],
                    ['level' => 1, 'nodeNo' => 2, 'sequence' => [1,3,4,5]],
                ]
            ]
        ];
        $I->sendPOST('/tree/svg', $json);
        $I->seeResponseCodeIs(HttpCode::OK);
        $I->canSeeResponseIsXml();
        $I->seeHttpHeader('Content-type', 'image/svg+xml');
    }

    public function requestSvgTreeWithoutAcceptedType(ApiTester $I)
    {
        $I->wantTo('Request for SVG Tree without provided Accept header');
        $json = [
            'treeParameters' => [
                'treeDepth' => 2,
                'treeBranchSize' => 2
            ]
        ];
        $I->sendPOST('/tree/svg', $json);
        $I->seeResponseCodeIs(HttpCode::OK);
        $I->seeHttpHeader('Content-type', 'image/svg+xml');
    }

    public function requestSvgTreeWithCommands(ApiTester $I)
    {
        $I->wantTo('Request for SVG Tree with provided commands');
        $json = [
            'treeParameters' => [
                'treeDepth' => 2,
                'treeBranchSize' => 2
            ],
            'commands' => [
                'swap' => [
                    ['level' => 0, 'nodeNo' => 1, 'sequence' => [1,3,4,5]],
                    ['level' => 1, 'nodeNo' => 2, 'sequence' => [1,3,4,5]],
                ]
            ]
        ];
        $I->sendPOST('/tree/svg', $json);
        $I->seeResponseCodeIs(HttpCode::OK);
    }

    public function requestSvgTreeWithInvalidCommands(ApiTester $I)
    {
        $I->wantTo('Request for SVG Tree with not provided config');
        $json = [
            'treeParameters' => [
                'treeDepth' => 2,
                'treeBranchSize' => 2
            ],
            'commands' => [
                'swap' => [
                    ['level' => 43, 'nodeNo' => 4, 'sequence' => [1,3,4,5]]
                ]
            ]
        ];
        $I->sendPOST('/tree/svg', $json);
        $I->seeResponseCodeIs(HttpCode::BAD_REQUEST);
        $I->canSeeResponseIsJson();
    }

    public function requestSvgTreeWithoutConfig(ApiTester $I)
    {
        $I->wantTo('Request for SVG Tree with not provided config');
        $json = [
            'treeParameters' => [
                'treeDepth' => 2,
                'treeBranchSize' => 2
            ]
        ];
        $I->sendPOST('/tree/svg', $json);
        $I->seeResponseCodeIs(HttpCode::OK);
    }

    public function requestSvgTreeWithoutCommands(ApiTester $I)
    {
        $I->wantTo('Request for SVG Tree with not provided commands');

        $json = [
            'treeParameters' => [
                'treeDepth' => 2,
                'treeBranchSize' => 2
            ],
            'config' => [
                'nodeHeight' => 40,
                'nodeWidth' => 40,
            ]
        ];
        $I->sendPOST('/tree/svg', $json);
        $I->seeResponseCodeIs(HttpCode::OK);
    }

    public function requestSvgTreeWithoutTreeParameters(ApiTester $I)
    {
        $I->wantTo('Request for SVG Tree without JSON content configuration');
        $json = [
            'config' => [
                'nodeHeight' => 40,
		        'nodeWidth' => 40,
            ]
        ];
        $I->sendPOST('/tree/svg', $json);
        $I->seeResponseCodeIs(HttpCode::BAD_REQUEST);
        $I->canSeeResponseIsJson();
    }

    public function requestSvgTreeWithAcceptedPdf(ApiTester $I)
    {
        $I->wantTo('Request for SVG Tree with Accepted header application/pdf');
        $json = [
            'treeParameters' => [
                'treeDepth' => 2,
                'treeBranchSize' => 2
            ]
        ];
        $I->haveHttpHeader('Accept', 'application/pdf');
        $I->sendPOST('/tree/svg', $json);
        $I->seeResponseCodeIs(HttpCode::OK);
        $I->seeHttpHeader('Content-type', 'application/pdf');
    }

    public function requestSvgTreeWithAcceptedDocx(ApiTester $I)
    {
        $docxType = 'application/vnd.openxmlformats-officedocument.wordprocessingml.document';
        $I->wantTo('Request for SVG Tree with Accepted header ' . $docxType);
        $json = [
            'treeParameters' => [
                'treeDepth' => 2,
                'treeBranchSize' => 2
            ]
        ];
        $I->haveHttpHeader('Accept', $docxType);
        $I->sendPOST('/tree/svg', $json);
        $I->seeResponseCodeIs(HttpCode::OK);
        $I->seeHttpHeader('Content-type', $docxType);
    }
}
