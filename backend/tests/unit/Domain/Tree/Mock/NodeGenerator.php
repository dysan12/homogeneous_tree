<?php
/**
 * User: michal_lap
 * Date: 25.11.18
 * Email: michalgaj@onet.pl
 */

namespace App\Tests\unit\Domain\Tree\Mock;


use App\Domain\Tree\Node\Generator;
use App\Domain\Tree\Node\Internal;
use App\Domain\Tree\Node\Leaf;

class NodeGenerator extends Generator
{
    /** @var int  */
    private $counter = 2;

    public function generateLeafs(int $childrenQuantity): array
    {
        $leafs = [];
        for ($i = 1; $i <= $childrenQuantity; $i++) {
            $leafs[] = new Leaf($this->counter++);
        }

        return $leafs;
    }

    public function generateInternals(int $depth, int $childrenQuantity): array
    {
        $internals = [];
        for ($i = 1; $i <= $childrenQuantity; $i++) {
            $counter = (($this->counter - 1) * $childrenQuantity) + 2;
            $internals[] = new Internal($this->counter++, $depth, $childrenQuantity, $this->cloneWithCounter($counter));
        }

        return $internals;
    }

    private function cloneWithCounter(int $counter): self
    {
        $generator = clone $this;
        $generator->counter = $counter;

        return $generator;
    }
}