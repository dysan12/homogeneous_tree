<?php
/**
 * User: Michal Gaj
 * Date: 09.11.18
 * E-mail: michalgaj@onet.pl
 */

use App\Domain\Tree\Node\Generator;

class InternalTest extends \Codeception\Test\Unit
{
    public function _before()
    {
        $this->nodeGenerator = $this->make(Generator::class,
            [
            ]
        );
    }

    public function testGetNo()
    {

    }

    public function test__construct()
    {

    }

    public function testGetChildren_WithDepthEqualTo1_ReturnsNodesWithoutChildren()
    {
    }
}
