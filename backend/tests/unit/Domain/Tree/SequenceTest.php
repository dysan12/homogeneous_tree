<?php
/**
 * User: michal_lap
 * Date: 21.11.18
 * Email: michalgaj@onet.pl
 */

use App\Domain\Tree\Operation\Swap\Sequence;

class SequenceTest extends \Codeception\Test\Unit
{
    /**
     * @dataProvider sequencesDataProvider
     */
    public function testCreateFromString(string $strSequence, string $delimiter, int $sequenceNumbersQuantity)
    {
        $sequence = Sequence::createFromString($strSequence, $delimiter);

        $this->assertCount($sequenceNumbersQuantity, $sequence->getSequence());
    }

    public function sequencesDataProvider(): array
    {
        return [
          ['4 3 6 7 2 8 2', ' ', 7],
          ['54/534/4/23/65', '/', 5],
          ['987;875;63433;34343', ';', 4]
        ];
    }

    /**
     * @expectedException \InvalidArgumentException
     */
    public function testCreateFromString_WithStringContainingString_ThrowsException()
    {
        $delimiter = '/';
        $stringSequence = implode($delimiter, [213,2312,123,'string']);

        Sequence::createFromString($stringSequence, $delimiter);
    }

    /**
     * @expectedException \InvalidArgumentException
     */
    public function testCreateFromString_WithStringContainingNegativeNumber_ThrowsException()
    {
        $delimiter = '/';
        $stringSequence = implode($delimiter, [213,2312,123,-2]);

        Sequence::createFromString($stringSequence, $delimiter);
    }
}
