<?php
/**
 * User: Michal Gaj
 * Date: 09.11.18
 * E-mail: michalgaj@onet.pl
 */

use App\Domain\Tree\Node\Internal;
use App\Domain\Tree\Node\Leaf;
use App\Domain\Tree\Node\Generator;

class NodeGeneratorTest extends \Codeception\Test\Unit
{

    public function testGenerateLeafs()
    {
        $generator = new Generator();

        $leafs = $generator->generateLeafs(5);

        $this->assertCount(5, $leafs);
        foreach ($leafs as $leaf) {
            $this->assertInstanceOf(Leaf::class, $leaf);
        }
    }

    public function testGenerateInternals()
    {
        $generator = new Generator();

        $internals = $generator->generateInternals(1, 5);
        $this->assertCount(5, $internals);

        foreach ($internals as $internal) {
            $this->assertInstanceOf(Internal::class, $internal);
        }
    }
}
