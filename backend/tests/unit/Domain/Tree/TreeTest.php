<?php
/**
 * User: Michal Gaj
 * Date: 09.11.18
 * E-mail: michalgaj@onet.pl
 */

use App\Common\Types\PositiveInteger;
use App\Domain\Tree\Service\TreeParametersInterface;
use App\Domain\Tree\Tree;
use App\Domain\Tree\Node;
use App\Tests\unit\Domain\Tree\Mock\NodeGenerator;

class TreeTest extends \Codeception\Test\Unit
{
    public function testCreate()
    {
        $treeDepth = 3;
        $branchSize = 3;

        $tree = $this->getTree($treeDepth, $branchSize);

        $nodesQuantity = $this->getNodesQuantity($treeDepth, $branchSize);
        $root = $tree->getRoot();

        $this->assertEquals($nodesQuantity, $this->countNodeSons($root) + 1); // + 1 for main root node
    }

    /**
     * @dataProvider nodePositionsDataProvider
     */
    public function testGetSon_With63Tree(Node\VO\Position $position)
    {
        $treeDepth = 6;
        $branchSize = 3;
        $requestedPositionNo =
            $this->getNodesQuantity($position->getLevel()->getNumber(), $branchSize) - // nodes quantity to position level
            $branchSize ** $position->getLevel()->getNumber() + // level nodes quantity
            $position->getNodeNo()->getNumber();

        $tree = $this->getTree($treeDepth, $branchSize);
        $son = $tree->getSon($position);

        $this->assertEquals($requestedPositionNo, $son->getNo());
    }

    /**
     * @dataProvider nodePositionsDataProvider
     */
    public function testGetSon_With64Tree(Node\VO\Position $position)
    {
        $treeDepth = 6;
        $branchSize = 4;
        $requestedPositionNo =
            $this->getNodesQuantity($position->getLevel()->getNumber(), $branchSize) - // nodes quantity to position level
            $branchSize ** $position->getLevel()->getNumber() + // level nodes quantity
            $position->getNodeNo()->getNumber();

        $tree = $this->getTree($treeDepth, $branchSize);
        $son = $tree->getSon($position);

        $this->assertEquals($requestedPositionNo, $son->getNo());
    }

    /**
     * @expectedException \App\Domain\Exception\SonPositionException
     */
    public function testGetSon_WithNotExistingPosition_ThrowsException()
    {
        $tree = $this->getTree(4, 4);
        $tree->getSon(Node\VO\Position::createFromInts(5, 3));
    }

    public function nodePositionsDataProvider(): array
    {
        return [
            0 => [Node\VO\Position::createFromInts(0, 1)],
            1 => [Node\VO\Position::createFromInts(1, 1)],
            2 => [Node\VO\Position::createFromInts(1, 2)],
            3 => [Node\VO\Position::createFromInts(2, 2)],
            4 => [Node\VO\Position::createFromInts(2, 6)],
            5 => [Node\VO\Position::createFromInts(2, 8)],
            6 => [Node\VO\Position::createFromInts(3, 11)],
            7 => [Node\VO\Position::createFromInts(3, 18)],
            8 => [Node\VO\Position::createFromInts(3, 27)],
            9 => [Node\VO\Position::createFromInts(4, 12)],
            10 => [Node\VO\Position::createFromInts(4, 65)],
            11 => [Node\VO\Position::createFromInts(4, 77)],
            12 => [Node\VO\Position::createFromInts(5, 4)],
            13 => [Node\VO\Position::createFromInts(5, 100)],
            14 => [Node\VO\Position::createFromInts(5, 211)],
            15 => [Node\VO\Position::createFromInts(6, 65)],
            16 => [Node\VO\Position::createFromInts(6, 158)],
            17 => [Node\VO\Position::createFromInts(6, 688)],
        ];
    }

    private function getTree(int $treeDepth, int $branchSize): Tree
    {
        $nodeGenerator = new NodeGenerator();
        /** @var TreeParametersInterface $treeParameters */
        $treeParameters = $this->makeEmpty(TreeParametersInterface::class, [
            'getTreeDepth' => new PositiveInteger($treeDepth),
            'getTreeBranchSize' => new PositiveInteger($branchSize)
        ]);

        return new Tree($treeParameters, $nodeGenerator);
    }

    private function countNodeSons(Node\ChildrenableInterface $node): int
    {
        $currentQuantity = 0;
        foreach ($node->getChildren() as $child) {
            $currentQuantity++;
            if ($child instanceof Node\ChildrenableInterface) {
                $currentQuantity += $this->countNodeSons($child);
            }
        }

        return $currentQuantity;
    }

    private function getNodesQuantity(int $treeDepth, int $branchSize): int
    {
        $quantity = 0;
        for ($i = 0; $i <= $treeDepth; $i++) {
            $quantity += $branchSize ** $i;
        }

        return $quantity;
    }

}
