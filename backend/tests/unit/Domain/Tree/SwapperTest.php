<?php
/**
 * User: michal_lap
 * Date: 21.11.18
 * Email: michalgaj@onet.pl
 */

use App\Common\Collection\PositiveNumbersCollection;
use App\Common\Types\PositiveNumber;
use App\Domain\Tree\Node\Generator;
use App\Domain\Tree\Operation\Swap\Sequence;
use App\Domain\Tree\Operation\Swap\Swapper;
use App\Domain\Tree\Node;

class SwapperTest extends \Codeception\Test\Unit
{
    /** @var Swapper */
    private $swapper;

    public function _before()
    {
        $this->swapper = new Swapper();
    }

    /**
     * @dataProvider swapDataProvider
     */
    public function testSwap(Sequence $sequence, array $startChildrenOrders, array $expectedChildrenOrders)
    {
        $leafs = [];
        foreach($startChildrenOrders as $order) {
            $leafs[] = new Node\Leaf($order);
        }
        /** @var Generator $nodeGenerator */
        $nodeGenerator = $this->make(Generator::class, [
            'generateLeafs' => $leafs,
        ]);

        $childrenQuantity = count($nodeGenerator->generateLeafs(9999));
        $internalNode = new Node\Internal(1, 1, $childrenQuantity, $nodeGenerator);

        $swappedNode = $this->swapper->swap($internalNode, $sequence);
        $children = $swappedNode->getChildren();
        $counter = 0;
        foreach ($expectedChildrenOrders as $index => $childOrder) {
            $child = $children[$index];
            if ($child instanceof Node\OrderableInterface) {
                $this->assertEquals($childOrder, $child->getNo());
                $counter++;
            }
        }
        $this->assertEquals($childrenQuantity, $counter);
    }

    public function swapDataProvider(): array
    {
        return [
          [$this->createSequence([3, 2, 4, 1]), [1, 2, 3, 4], [4, 3, 1, 2]],
          [$this->createSequence([1, 2, 3, 4]), [1, 2, 3, 4], [4, 1, 2, 3]],
          [$this->createSequence([4, 3, 2, 1]), [1, 2, 3, 4], [2, 3, 4, 1]],
          [$this->createSequence([1, 3, 2, 4]), [1, 2, 3, 4], [4, 3, 1, 2]],//
          [$this->createSequence([3, 5, 1, 2, 4]), [1, 2, 3, 4, 5], [5, 1, 4, 2, 3]],
          [$this->createSequence([1, 3, 2]), [2, 3, 1], [3, 1, 2]],
          [$this->createSequence([4, 2, 3, 1]), [4, 3, 2, 1], [1, 2, 4, 3]],
          [$this->createSequence([5, 1, 7, 6, 3, 2, 4]), [7, 6, 5, 4, 3, 2, 1], [1, 7, 4, 2, 6, 3, 5]],
          [$this->createSequence([3, 2, 4, 1]), [3, 4, 1, 2], [1, 2, 4, 3]],
          [$this->createSequence([33, 5, 7]), [1, 2, 3], [2, 3, 1]],
          [$this->createSequence([1, 2, 3, 4, 5, 6]), [2, 1], [2, 1]],
          [$this->createSequence([3, 3, 3, 3, 3, 3, 3]), [1, 2, 3], [1, 2, 3]],
          [$this->createSequence([5]), [5], [5]],
        ];
    }

    /**
     * @expectedException \App\Domain\Exception\SequenceSwappingException
     */
    public function testSwap_WithInvalidSequence_ThrowsException()
    {
        $leafs = [new Node\Leaf(5), new Node\Leaf(8)];
        /** @var Generator $nodeGenerator */
        $nodeGenerator = $this->make(Generator::class, [
            'generateLeafs' => $leafs,
        ]);
        $internalNode = new Node\Internal(1, 1, \count($leafs), $nodeGenerator);

        $this->swapper->swap($internalNode, $this->createSequence([1, 2, 3]));
    }

    /**
     * @expectedException \InvalidArgumentException
     */
    public function testSwap_WithNodeContainsNotOnlyOrderableChildren_ThrowsException()
    {
        $leafs = ['leaf1', 'leaf2'];
        /** @var Generator $nodeGenerator */
        $nodeGenerator = $this->make(Generator::class, [
            'generateLeafs' => $leafs,
        ]);
        $internalNode = new Node\Internal(1, 1, \count($leafs), $nodeGenerator);

        $this->swapper->swap($internalNode, $this->createSequence([1, 2, 3]));
    }

    private function createSequence(array $numbers): Sequence
    {
        $positiveNumbers = new PositiveNumbersCollection();
        foreach ($numbers as $number) {
            $positiveNumbers[] = new PositiveNumber($number);
        }

        return new Sequence($positiveNumbers);
    }
}
