<?php
/**
 * User: Michal Gaj
 * Date: 05.11.18
 * E-mail: michalgaj@onet.pl
 */

use App\Domain\Graph\Svg;
use App\Domain\Graph\Svg\SvgConfig;
use App\Domain\Tree\Node\Leaf;
use App\Domain\Tree\Node\Root;
use App\Domain\Tree\Tree;

class SvgGraphTest extends \Codeception\Test\Unit
{
    /**
     * @return Tree|object
     */
    private function getTree(): Tree
    {
        return $this->make(Tree::class, [
            'getBranchSize' => 2,
            'getDepth' => 2,
            'getRoot' => $this->make(Root::class, [
                'getChildren' => [
                    $this->make(Leaf::class, ['getNo' => 1]),
                    $this->make(Leaf::class, ['getNo' => 2])
                ],
                'getNo' => 1
            ])
        ]);
    }

    public function testGetRepresentation_WithEmptyConfig()
    {
        $graph = new Svg\Graph($this->getTree(), new SvgConfig());

    }
}
