<?php
/**
 * User: Michal Gaj
 * Date: 29.10.18
 * E-mail: michalgaj@onet.pl
 */

use App\Domain\Graph\Service\XmlConfigInterface;
use App\Domain\Graph\Xml;
use App\Domain\Tree\Node\Leaf;
use App\Domain\Tree\Node\Root;
use App\Domain\Tree\Tree;

class XmlGraphTest extends \Codeception\Test\Unit
{
    public function testGetRepresentation()
    {
        /** @var Tree $tree */
        $tree = $this->getTree();
        /** @var  XmlConfigInterface $config */
        $config = $this->make(Xml\XmlConfig::class, [
           'getChildTagName' => 'node'
        ]);

        $graph = new Xml\Graph($tree, $config);

        $this->assertEquals('<?xml version="1.0"?>
<tree><root no="1"><node no="1"/><node no="2"/></root></tree>
', $graph->getRepresentation());
    }

    public function testGetRepresentation_WithCustomizedNodeName_ReturnsCorrectXml(): void
    {
        /** @var Tree $tree */
        $tree = $this->getTree();
        /** @var XmlConfigInterface $config */
        $config = $this->make(Xml\XmlConfig::class, [
            'getChildTagName' => 'customized_node_name'
        ]);

        $graph = new Xml\Graph($tree, $config);

        $this->assertEquals('<?xml version="1.0"?>
<tree><root no="1"><customized_node_name no="1"/><customized_node_name no="2"/></root></tree>
', $graph->getRepresentation());
    }

    /**
     * @return Tree|object
     * @throws Exception
     */
    private function getTree(): Tree
    {
        return $this->make(Tree::class, [
            'getRoot' => $this->make(Root::class, [
                'getChildren' => [
                    $this->make(Leaf::class, ['getNo' => 1]),
                    $this->make(Leaf::class, ['getNo' => 2])
                ],
                'getNo' => 1
            ])
        ]);
    }
}
