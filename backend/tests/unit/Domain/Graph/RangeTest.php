<?php
/**
 * User: Michal Gaj
 * Date: 29.10.18
 * E-mail: michalgaj@onet.pl
 */

use App\Domain\Graph\Svg\Positioning\Range;

class RangeTest extends \Codeception\Test\Unit
{
    /**
     * @dataProvider parametersDataProvider
     */
    public function testGetMiddlePoint__ReturnsCorrectMiddlePoint(float $start, float $end, float $expected)
    {
        $range = new Range($start, $end, 1);

        $this->assertEquals($expected, $range->getMiddlePoint());
    }

    public function parametersDataProvider(): array
    {
        return [
          [10, 100, 55],
          [0, 1, 0.5],
          [1, 1.1, (1.1 + 1)/2],
        ];
    }

    /**
     * @expectedException \InvalidArgumentException
     */
    public function testCreateRange_WithDivisionSetTo0_ThrowsException()
    {
        new Range(0, 1, 0);
    }

    /**
     * @expectedException \InvalidArgumentException
     */
    public function testCreateRange_WithStartGreaterThanEnd_ThrowsException()
    {
        new Range(10, 1 , 1);
    }

    public function testHasNextRange__CorrectlyIndicateRangeState()
    {
        $division = 5;
        $range = new Range(0, 10, $division);
        for ($i = 0; $i < $division; $i++) {
            $this->assertEquals(true, $range->hasNextSubRange());
            $range->getNextSubRange();
        }
        $this->assertEquals(false, $range->hasNextSubRange());
    }

    public function testGetNextRange__ReturnsNextRangeWithCorrectStartPoint()
    {
        $start = 0;
        $end = 100;
        $division = 4;

        $range = new Range($start, $end, $division);
        for ($i = 0; $i < $division; $i++) {
            $nextRange = $range->getCurrentSubRange();
            $this->assertEquals($start + ($i * $end / $division), $nextRange->getStart());
            $range->getNextSubRange();
        }
    }

    /**
     * @expectedException  \OutOfBoundsException
     */
    public function testGetNextRange_WithUsedAllSubRanges_ThrowsException()
    {
        $range = new Range(0, 100, 1);
        $range->getNextSubRange();
        $range->getNextSubRange();
    }

    public function testGetCurrentSubRange()
    {
        $range = new Range(0, 100, 4);

        $current = $range->getCurrentSubRange();

        $this->assertEquals(0, $current->getStart());
        $this->assertEquals(100 / 4, $current->getEnd());
    }
}
