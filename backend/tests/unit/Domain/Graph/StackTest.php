<?php
/**
 * User: Michal Gaj
 * Date: 05.11.18
 * E-mail: michalgaj@onet.pl
 */

use App\Domain\Graph\Svg\Stack;

class StackTest extends \Codeception\Test\Unit
{
    public function testGet()
    {
        $stack = new Stack();

        $this->assertCount(0, $stack->get(1));

        $index = 3;
        $stack->add($index, 'ex1');
        $stack->add($index, 'ex2');

        $this->assertCount(2, $stack->get($index));
    }

    public function testAdd()
    {
        $stack = new Stack();

        $index = 3;
        $element = 'ex1';
        $stack->add($index, $element);

        $collection = $stack->get($index);

        $this->assertCount(1, $collection);
        $this->assertEquals($collection[0], $element);
    }

    public function testIterator()
    {
        $stack = new Stack();

        $elemNumber = 4;
        for ($i = 0; $i < $elemNumber; $i++) {
            $stack->add($i, 'ex' . $i);
        }

        $counter = 0;
        foreach ($stack as $key => $collection)
        {
            $this->assertEquals('ex' . $counter++, $collection[0]);
        }

        $this->assertEquals($counter, $elemNumber);
    }
}
