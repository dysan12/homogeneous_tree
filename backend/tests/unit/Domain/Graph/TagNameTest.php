<?php
/**
 * User: Michal Gaj
 * Date: 02.11.18
 * E-mail: michalgaj@onet.pl
 */

use App\Domain\Graph\Xml\VO\TagName;

class TagNameTest extends \Codeception\Test\Unit
{
    /**
     * @expectedException \InvalidArgumentException
     */
    public function testTagName_WithNameStartWithDigit_ThrowsException()
    {
        new TagName('9name');
    }

    /**
     * @expectedException \InvalidArgumentException
     */
    public function testTagName_WithXmlBeginningName_ThrowsException()
    {
        new TagName('xmlSomeTag');
    }

    /**
     * @expectedException \InvalidArgumentException
     */
    public function testTagName_WithNameStartedByDot_ThrowsException()
    {
        new TagName('.someTag');
    }

    public function testTagName_WithNameStartedByUnderscore_CreatesCorrectly()
    {
        $tag = '_somoe_tag';
        $tagName = new TagName($tag);

        $this->assertEquals($tag, $tagName->getTagName());
    }
}
