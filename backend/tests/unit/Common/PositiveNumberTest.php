<?php

use App\Common\Types\PositiveNumber;

/**
 * User: Michal Gaj
 * Date: 02.11.18
 * E-mail: michalgaj@onet.pl
 */


class PositiveNumberTest extends \Codeception\Test\Unit
{
    public function testGetNumber()
    {
        $float = 1.22;

        $number = new PositiveNumber($float);

        $this->assertEquals($float, $number->getNumber());
    }

    /**
     * @expectedException \InvalidArgumentException
     */
    public function testCreatePositiveNumber_WithNegativeFloat_ThrowsException()
    {
        new PositiveNumber(-1.33);
    }
}
