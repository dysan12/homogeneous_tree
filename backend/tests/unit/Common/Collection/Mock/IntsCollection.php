<?php
/**
 * User: michal_lap
 * Date: 21.11.18
 * Email: michalgaj@onet.pl
 */

namespace App\Tests\unit\Common\Collection\Mock;



use App\Common\Collection\AbstractCollection;

class IntsCollection extends AbstractCollection
{
    protected function getType(): string
    {
        return \gettype(3);
    }
}