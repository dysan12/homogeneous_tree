<?php

use App\Common\Collection\AbstractCollection;
use App\Tests\unit\Common\Collection\Mock\IntsCollection;

/**
 * User: michal_lap
 * Date: 21.11.18
 * Email: michalgaj@onet.pl
 */


class AbstractCollectionTest extends \Codeception\Test\Unit
{
    /** @var int[]|AbstractCollection */
    private $intsCollection;
    /** @var stdClass[]|AbstractCollection */
    private $stdClassesCollection;

    public function _before()
    {
        $this->intsCollection = new class extends AbstractCollection {
            protected function getType(): string
            {
                return \gettype(1);
            }
        };

        $this->stdClassesCollection = new class extends AbstractCollection {
            protected function getType(): string
            {
                return stdClass::class;
            }
        };
    }

    public function testCreateCollection_WithInitialArray()
    {
        $initialArray = [1,2,34];
        $collection = new IntsCollection($initialArray);

        $this->assertCount(count($initialArray), $collection);
    }

    /**
     * @expectedException \InvalidArgumentException
     */
    public function testCreateCollection_WithInvalidInitialArray_ThrowsException()
    {
        $initialArray = [1,2,'321'];
        $collection = new IntsCollection($initialArray);
    }

    public function testCountable()
    {
        $this->assertCount(0, $this->intsCollection);

        $this->intsCollection[] = 4;

        $this->assertCount(1, $this->intsCollection);
    }

    public function testAddElement()
    {
        $elements = [4, 6, 8];
        $intCollection = $this->intsCollection;

        foreach ($elements as $element) {
            $intCollection[] = $element;
        }

        $this->assertEquals($elements[0], $intCollection[0]);
        $this->assertEquals($elements[1], $intCollection[1]);
        $this->assertEquals($elements[2], $intCollection[2]);
    }

    /**
     * @expectedException \InvalidArgumentException
     */
    public function testAddElement_WithInvalidType_ThorwsException()
    {
        $this->stdClassesCollection[] = 'string';
    }

    public function testIteration()
    {
        $elements = [new stdClass(), new stdClass()];
        $classesCollection = $this->stdClassesCollection;

        foreach ($elements as $element) {
            $classesCollection[] = $element;
        }

        $counter = 0;
        foreach ($classesCollection as $key => $class) {
            $this->assertEquals($elements[$key], $class);
            $counter++;
        }
        $this->assertEquals(count($elements), $counter);
    }
}
