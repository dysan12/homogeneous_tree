# Drzewo jednorodne

#### Operacje na strukturach drzewiastych

Drzewo jednorodne to aplikacja służąca do wizualizacji i przetwarzania struktury drzewiastej, jaką jest drzewo jednorodne. Program wykonuje następujące obliczenia w grupie automorfizmów drzewa jednorodnego dla zadanego korzeniem wysokości *n*:

  - mnożenie
  - odwracanie
  - komutatory
  - "potęgowanie"
  
 Aplikacja prezentuje graficznie wyniki oraz posiada opcje importu i eksportu danych z/do formatu tekstowego oraz eksport do pliku PDF.

#### Funkcjonalności

  - Wizualizacja drzewa jednorodnego na podstawie zadanych danych: *n* - ilość poziomów drzewa oraz *p* - ilość rozgałęzień na każdym wierzchołku
  - Wykonywanie kombinacji drzewa (operacje na drzewie: mnożenie, odwracanie, komutatory i "potęgowanie")
  - Historia wykonanych zmian
  - Eksport danych do pliku tekstowego
  - Import danych z pliku tekstowego
  - Eksport danych do pliku PDF
  
#### Instalacja    
  - Uruchomić skrypt inicjalizujący `init.sh` poleceniem `sudo sh scripts/init.sh` będąc w root projektu
  
#### Uruchamianie
  - Aplikację uruchamiać za pomocą `sudo sh scripts/docker_start.sh `