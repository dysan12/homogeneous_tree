document.getElementById('gen').
    addEventListener('click', function() {
      getMainTree(document.getElementById('parameterN').value,
          document.getElementById('parameterP').value, "noneAction");
    });

document.getElementById('buttonMulti').addEventListener('click', function() {
  getMainTree(document.getElementById('parameterN').value,
      document.getElementById('parameterP').value, "multi");
});

document.getElementById('buttonReversal').addEventListener('click', function() {
  getMainTree(document.getElementById('parameterN').value,
      document.getElementById('parameterP').value, "reversal");
});

document.getElementById('buttonComutator').addEventListener('click', function() {
  operationComutator();
});

document.getElementById('buttonExponen').addEventListener('click', function() {
  getMainTree(document.getElementById('parameterN').value,
      document.getElementById('parameterP').value, "exponen");
});

document.getElementById("buttonExport").addEventListener("click",function () {
  exportTree();
});

document.getElementById("parameterP").addEventListener("change",function () {
  document.getElementById("valueP").textContent = this.value;
});

document.getElementById("parameterN").addEventListener("change",function () {
  document.getElementById("valueN").textContent = this.value;
});


let request = {swap: []};
let requestConJson;

function getMainTree(height, branches, action) {
  const xhttp = new XMLHttpRequest();
  let swaprequest;

  if(action === "noneAction") {
    request = {swap: []};
  }
  if(action === "multi" || action === "exponen") {
    swaprequest = generateCommandsToSwapTree(
        parseInt(document.getElementById('parameterP').value), action);
  }

  if(swaprequest === null || action === "toPDF" || action === "toDOCX" ){

    swaprequest = request;
  }

  if(action === "reversal") {

    swaprequest = reversalQuest();
  }

  if(action === "comutator")
  {
    if(document.getElementById("buttonComutator").textContent !== "Komutatory" )
    {
      swaprequest = request;
    }
    else {
      swaprequest = "";
      console.log(swaprequest);
    }
  }

  xhttp.open('POST', 'http://api.tree.test:8081/tree/svg', true);
  xhttp.onreadystatechange = function() {
    if (this.readyState === 4 && this.status === 200) {
      if(action === "toPDF")
      {
        exportPDF(this.response);
      } else if (action === "toDOCX"){
        exportDOCX(this.response);
      }
      else {
        document.getElementById('svgDiv').innerHTML = this.responseText;
        centerTree(document.getElementById('svgDiv').clientWidth,
            document.getElementById('svgDiv').clientHeight);
        addEventsForEllipses(branches, height);
      }
    }
  };

  if(action !== "imTXT") {
    requestConJson = JSON.stringify({
      'treeParameters': {'treeDepth': height, 'treeBranchSize': branches},
      'config': {
        'nodeHeight': 40,
        'nodeWidth': 40,
        'gapHorizontalSize': 5,
        'gapVerticalSize': 70,
      },
      'commands': swaprequest,
    });
  }
  if(action === "toPDF") {
     xhttp.responseType = 'blob';
     xhttp.setRequestHeader('Accept','application/pdf');
  }
  else if(action === "toDOCX"){
    xhttp.responseType = 'blob';
    xhttp.setRequestHeader('Accept','application/vnd.openxmlformats-officedocument.wordprocessingml.document');
  }

  xhttp.send(requestConJson);
}

function addEventsForEllipses(p, n) {
  let svg = document.getElementsByTagName('svg')[0];
  let ellipses = svg.getElementsByTagName('ellipse');

  for (let i = 0; i < ellipses.length; i++) {
    if (ellipses[i].getAttribute('cy') < n * 100) {
      ellipses[i].addEventListener('click', function() {
        createInput(parseInt(this.getAttribute('cx')) - 33,
            parseInt(this.getAttribute('cy')) + 30);
      });
    }
  }
}

function createInput(left, top) {

  const divSVG = document.getElementById('svgDiv');
  const level = parseInt(top / 100);
  const name = createInputName(level,left+33,document.getElementById('parameterP').value,document.getElementById('parameterN').value,divSVG.getElementsByTagName("svg").item(0).getAttribute("width"));
  const inputsInSVG = divSVG.getElementsByTagName('input');

  for (let i = 0; i < inputsInSVG.length; i++) {
    if (inputsInSVG[i].getAttribute('name') === name) {
      return;
    }
  }

  const input = document.createElement('INPUT');
  input.setAttribute('type', 'text');
  input.setAttribute('name', name);
  input.setAttribute('class', 'sequence');
  input.style.top = top + 'px';
  input.style.left = left + 'px';
  divSVG.appendChild(input);
}

function centerTree(widthTree, heightTree) {
  const widthPage = window.innerWidth;
  const heightPage = document.getElementById('idMain').clientHeight;
  const svg = document.getElementById('svgDiv');

  svg.style.marginLeft = 0 + 'px';
  svg.style.marginTop = 0 + 'px';

  if (widthTree < widthPage) {
    svg.style.marginLeft = (widthPage - widthTree) / 2 + 'px';
    svg.style.marginTop = (heightPage - heightTree) / 2 + 'px';
  }
}

function createInputName(level,left,parametrP,parametrN,svgWidth) {

  if(level === 0) {
    return level+",1";
  } else {
      let nodeNo = 0;
      let intervalsCount = Math.pow(parametrP, level);
      let singleInteval = svgWidth / intervalsCount;
      let intervals = [];
      intervals[0] = singleInteval;

      for (let j = 1; j < intervalsCount; j++) {
        intervals[j] = intervals[j-1]+singleInteval;
      }

      for(let i = 0; i < intervals.length; i++) {
        if(left < intervals[i]) {
          nodeNo = i + 1;
          break;
        }
      }
      return level+","+nodeNo;
  }
}

function generateCommandsToSwapTree(parameterP, action) {
  const inputs = document.getElementById('svgDiv').getElementsByTagName('input');
  let swapLenght = request.swap.length;
  let numberCorrectInput =0;
  let arrayOptionsInput =[];
  let exponent = document.getElementById('valueExp').value;

  if (action === "exponen" && swapLenght <= 1048)
  {
    arrayOptionsInput = request.swap;
    for(let g = 1; g < exponent ; g++) {

      for (let i = 0; i < swapLenght; i++) {
        request.swap[i + swapLenght*g] = arrayOptionsInput[i];
      }
    }

    return request;
  } else if (action === "exponen" && swapLenght > 1048){
    alert("Ups... więcej nie dam rady się rozmnażać");
  }

  for (let i = 0; i < inputs.length; i++) {
    let inputCorrect = true;
    let name = inputs[i].getAttribute('name');
    let position = name.split(',');
    let level = parseInt(position[0]);
    let nodeNo = parseInt(position[1]);
    let requestLevel = {level: level, nodeNo: nodeNo, sequence: []};
    let valueInput = inputs[i].value.split(',');

    multiAction(valueInput, parameterP, requestLevel);


    if (valueInput.length <= parameterP && valueInput[0] !== "" && inputCorrect === true) {
        arrayOptionsInput[numberCorrectInput] = requestLevel;
        numberCorrectInput++;
    } else if (inputCorrect=== false){

    }
  }

  arrayOptionsInput.reverse();
  for (let k = 0; k <arrayOptionsInput.length; k++ ) {
    request.swap[swapLenght + k] = arrayOptionsInput[k];
  }

  if (request.swap[swapLenght]) {
    return request;
  } else {
    return null;
  }
}

function multiAction(valueInput, parameterP, requestLevel){
  let inputCorrect = true;

  for (let j = 0; j < valueInput.length; j++) {
    if (parseInt(valueInput[j]) > 0 && parseInt(valueInput[j]) <=
        parameterP) {
      requestLevel.sequence[j] = parseInt(valueInput[j]);
    } else {
      alert("Wpisano niepoprawną permutację");
      inputCorrect = false;
      return inputCorrect;
    }
  }
  return requestLevel;
}


function reversalQuest() {
  let reversalRequest = JSON.parse(JSON.stringify(request));
  // let reversalRequest = {...request};

  for(let j=0; j < request.swap.length; j++) {
    reversalRequest.swap[j].sequence = [];

    for (let i = request.swap[j].sequence.length -1; i >= 0; i--) {

      reversalRequest.swap[j].sequence.push(request.swap[j].sequence[i]);
    }
  }
  return reversalRequest;
}

function exportTree() {
  let div = document.createElement("div");
  div.setAttribute("id","exportDiv");

  let frame = document.createElement("div");
  frame.setAttribute("id","frame");


  let select = document.createElement("select");
  select.setAttribute("id","select");


  let optionPDF = document.createElement("option");
  optionPDF.textContent = "Export do PDF";
  optionPDF.value = "pdf";
  let optionDOCX = document.createElement("option");
  optionDOCX.textContent = "Export do DOCX";
  optionDOCX.value = "docx";
  let optionTXT = document.createElement("option");
  optionTXT.textContent = "Export do TXT";
  optionTXT.value = "txt";

  let buttonClose = document.createElement("button");
  buttonClose.setAttribute("type","button");
  buttonClose.setAttribute("id", "buttonClose");
  buttonClose.textContent = "Zamknij";

  buttonClose.onmouseover = function() {
    this.style.backgroundColor = "#555555";
    this.style.color = "white";
  };
  buttonClose.onmouseleave = function() {
    this.style.backgroundColor = "white";
    this.style.color = "black";
  };
  buttonClose.addEventListener("click",function () {
    document.body.removeChild(div);
  });

  let buttonOK = document.createElement("button");
  buttonOK.setAttribute("type","button");
  buttonOK.setAttribute("id","buttonOK");
  buttonOK.textContent="OK";

  buttonOK.onmouseover = function() {
    this.style.backgroundColor = "#555555";
    this.style.color = "white";
  };
  buttonOK.onmouseleave = function() {
    this.style.backgroundColor = "white";
    this.style.color = "black";
  };
  buttonOK.addEventListener("click",function () {
    switch (select.options[select.selectedIndex].value) {
      case "pdf":
      {
        getMainTree(document.getElementById('parameterN').value,
            document.getElementById('parameterP').value, "toPDF");
        document.body.removeChild(div);
        break;
      }
      case "docx":
      {
        getMainTree(document.getElementById('parameterN').value,
            document.getElementById('parameterP').value, "toDOCX");
        document.body.removeChild(div);
        break;
      }
      case "txt":
      {
        exportTXT();
        document.body.removeChild(div);
        break;
      }
    }
  });

  select.appendChild(optionPDF);
  select.appendChild(optionDOCX);
  select.appendChild(optionTXT);

  frame.appendChild(select);
  frame.appendChild(buttonOK);
  frame.appendChild(buttonClose);

  div.appendChild(frame);
  document.body.appendChild(div);

}

function operationComutator() {
  let parameterP = parseInt(document.getElementById('parameterP').value);
  let buttonComutatioText = document.getElementById("buttonComutator").textContent;
  let action = "multi";

  if(buttonComutatioText === "Komutatory")
  {
    request = {swap: []};
    document.getElementById("buttonComutator").textContent = "Zatwierdź pierwszą permutację";

    getMainTree(document.getElementById('parameterN').value,
        document.getElementById('parameterP').value, "comutator");

  }
  else if(buttonComutatioText === "Zatwierdź pierwszą permutację")
  {
    let valueinput = generateCommandsToSwapTree(parameterP, action);
    if(valueinput !== null) {
      request = valueinput;
      document.getElementById("buttonComutator").textContent = "Zatwierdź drugą permutację";
    }
  }
  else if(buttonComutatioText === "Zatwierdź drugą permutację")
  {
    let valueinput = generateCommandsToSwapTree(parameterP, action);
    if(valueinput !== null) {
      request = valueinput;
      document.getElementById("buttonComutator").textContent = "Oblicz permutację";
    }
  }
  else {
    let reversalRequest;
    reversalRequest = reversalQuest();

    for (let i=0 ; i<request.swap.length ; i++)
    {
      reversalRequest.swap.push(request.swap[i]);
    }

    request = reversalRequest;
    getMainTree(document.getElementById('parameterN').value,
        document.getElementById('parameterP').value, "comutator");
    document.getElementById("buttonComutator").textContent = "Komutatory";

  }
}

function exportPDF(response) {
  let file = window.URL.createObjectURL(response);
  let a = document.createElement("a");
  a.href = file;
  a.download = response.name || "tree.pdf"; // default file name
  document.body.appendChild(a);
  a.click();
  document.body.removeChild(a);
}

function exportDOCX(response) {
  let file = window.URL.createObjectURL(response);
  let a = document.createElement("a");
  a.href = file;
  a.download = response.name || "tree.docx"; // default file name
  document.body.appendChild(a);
  a.click();
  document.body.removeChild(a);
}

function exportTXT() {
  let a = document.createElement("a");
  let file = new Blob([requestConJson],{type: 'text/plain'});
  a.href = URL.createObjectURL(file);
  a.download = "tree.txt";
  document.body.appendChild(a);
  a.click();
  document.body.removeChild(a);
}

function importTXT(event) {
  let file = event.files[0], read = new FileReader();
  read.readAsBinaryString(file);

  read.onloadend = function() {
    let requestInFile = JSON.parse(JSON.stringify(read.result));
    requestConJson = requestInFile;

    getMainTree(document.getElementById('parameterN').value,
        document.getElementById('parameterP').value, "imTXT");
  };
}

document.getElementById("buttonImport").addEventListener("click", function() {
  document.getElementById("fileImport").click();
});

document.getElementById("fileImport").addEventListener("change",function() {
  importTXT(this);
});
